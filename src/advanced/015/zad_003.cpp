/*
 * Maciej Barć ; 64047
 *
 * *** queue ***
 *
 * 5. Napisz funkcję pobierającą od użytkownika imiona osób,
 *    chcących zapisać się do lekarza.
 *    Pobieranie zostanie zakończone gdy użytkownik wpisze wartość "0".
 *    Na końcu wyświetl ilość osób w kolejce,
 *    oraz osobę która jako pierwsza dostanie się do lekarza.
 */


#include <iostream>
#include <ostream>
#include <queue>


std::queue<std::string> my_queue;


void create_queue ( )
{
    std::string input;

    while ( 1 )
        {
            std::cout << "Podaj imię: ";
            std::cin >> input;

            if ( input != "0" )
                my_queue.push(input);
            else
                break;
        }

    std::cout << "Ilość osób w kolejce:     " << my_queue.size()  << std::endl;
    std::cout << "Pierwsza osoba w kolejce: " << my_queue.front() << std::endl;
}


int main ( )
{
    create_queue();


    return 0;
}
