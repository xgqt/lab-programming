/*
 * Maciej Barć ; 64047
 *
 * *** stack ***
 *
 * 3. Napisz funkcję, która pobierze od użytkownika cyfry składające się
 *    na liczbę zapisaną w systemie binarnymi zwróci liczbę jej
 *    odpowiadającą w systemie dziesiętnym.
 *    Skorzystaj ze stosu.
 *
 * 4. Napisz funkcję, dokonującej odwrotnej zamiany, niż w zadaniu 1.
 */


#include <iostream>
#include <stack>

#include "math.h"


std::stack <int> my_stack;


int binary_to_decimal ( )
{
    int digit;
    int num = 0;
    int i = 0;

    while ( 1 )
        {
            std::cout << "Podaj składnik liczby binarnej ... ";
            std::cin >> digit;

            if ( digit == 0 || digit == 1 )
                my_stack.push(digit);
            else
                break;
        }

    while ( ! my_stack.empty() )
        {
            num += my_stack.top() * pow(2, i);
            my_stack.pop();
            i ++;
        }

    return num;
}


int main ( )
{
    int _binary_to_decimal = binary_to_decimal();
    std::cout << "Podana liczba binarna po konwersji to: "
              << _binary_to_decimal << std::endl;


    return 0;
}
