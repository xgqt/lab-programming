/*
 * Maciej Barć ; 64047
 *
 * *** vector ***
 *
 * 1. Napisz funkcję, która pobierać będzie od użytkownika liczby całkowite,
 *    tak długo aż użytkownik poda 0.
 *    Liczby zapisuj do wektora o zasięgu globalnym.
 *
 * 2. Napisz funkcję, sprawdzającą czy podana liczba zawiera się w wektorze.
 */


#include <iostream>
#include <vector>


std::vector<int> my_vector;


void write_to_vector ( )
{
    int input;

    while ( 1 )
        {
            std::cout << "Podaj liczbę ... ";
            std::cin >> input;

            my_vector.push_back(input);

            if ( input == 0 )
                {
                    break;
                }
        }
}

void show_vector ( )
{
    for ( int i = 0; i < int(my_vector.size()); i ++ )
        {
            std::cout << "Index " << i << ": "
                      << my_vector[i] << std::endl;
        }
}

bool vector_contains ( int num )
{
    // for ( int i = 0; i < int(my_vector.size()); i ++ )
    //     {
    //         if ( my_vector[i] == num )
    //             return true;
    //     }

    for ( auto i : my_vector )
        {
            if ( i == num )
                return true;
        }

    return false;
}


int main ( )
{
    write_to_vector();

    std::cout << "Rozmiar pamięci wektora: "
              << my_vector.capacity() << std::endl;

    show_vector();


    if ( vector_contains(1) ) std::cout << "Zawiera 1" << std::endl;
    if ( vector_contains(2) ) std::cout << "Zawiera 2" << std::endl;
    if ( vector_contains(4) ) std::cout << "Zawiera 4" << std::endl;


    return 0;
}
