#include "dlugopis.hpp"

#include <iostream>


Dlugopis::Dlugopis (
                    std::string firma,
                    int czas_pisania,
                    int wysokosc,
                    int szerokosc,
                    int cena
                    )
{
    this->firma = firma;
    this->czas_pisania = czas_pisania;
    this->wysokosc = wysokosc;
    this->szerokosc = szerokosc;
    this->cena = cena;
}


void Dlugopis::zmien_firme ( std::string firma )
{
    this->firma = firma;
}

void Dlugopis::wypisz ( )
{
    std::cout
        << "Firma:        " << this->firma        << std::endl
        << "Czas pisania: " << this->czas_pisania << std::endl
        << "Wysokość:     " << this->wysokosc     << std::endl
        << "Szerokość:    " << this->szerokosc    << std::endl
        << "Cena:         " << this->cena         << std::endl;
}

Dlugopis Dlugopis::dluzszy_czas_pisania (
                                         Dlugopis dlugopis1,
                                         Dlugopis dlugopis2
                                         )
{
    if ( dlugopis1.czas_pisania > dlugopis2.czas_pisania )
        return dlugopis1;
    else
        return dlugopis2;
}
