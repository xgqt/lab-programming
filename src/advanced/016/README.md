# VisualStudio

VisualStudio podobno posiada wsparcie dla projektów w CMake
ale ten projekt nie był testowany w takim środowisku.


# Narzędzia

- POSIX-owy shell, np. bash lub mksh (nie fish)
- CMake (i GNU Make)

## Windows

### CMake

CMake może zostać zainstalowany przez VisualStudio
lub ze strony projektu:
https://cmake.org/

### Bash

Bash na Windowsie może zostać zainstalowany wraz z
instalacją Gita z oficjalnej strony:
https://gitforwindows.org/


# Kompilacja

Aby skompilować wykonaj te komendy w POSIX-owym shellu:

```bash
[ -d ./build ] && rm -r ./build
mkdir -p ./build && cd ./build
cmake .. && make -j5
```
