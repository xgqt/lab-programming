#pragma once

#include <iostream>


class Dlugopis
{
public:

    std::string firma;
    int czas_pisania;
    int wysokosc;
    int szerokosc;
    int cena;


    Dlugopis (
              std::string firma = "Bic",
              int czas_pisania = 1000,
              int wysokosc = 7,
              int szerokosc = 2,
              int cena = 2
              );


    void wypisz ( );

    void zmien_firme ( std::string );

    static Dlugopis dluzszy_czas_pisania (
                                          Dlugopis dlugopis1,
                                          Dlugopis dlugopis2
                                          );
};
