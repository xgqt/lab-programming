/*
 * Maciej Barć ; 64047
 *
 * Temat: długopis
 *
 * 1. Napisz program wczytujący od użytkownika rozmiar dwuwymiarowej
 *    tablicy dynamicznej, której liczba wierszy i kolumn jest taka sama.
 *    Tablicę uzupełnij liczbami które następnie pobierzesz od użytkownika.
 *    Wypisz na ekran największy element w tablicy oraz jego indeks.
 *    Do wykonania zadania użyj notacji wskaźnikowej.
 */


#include <iostream>


using namespace std;



int main ( )
{
    int size;

    cout << "Podaj rozmiar macierzy ... ",
        cin >> size;


    int ** tab = new int * [ size ];

    for ( int i = 0; i < size; i ++ )
        {
            * ( tab + i ) = new int [size];
        }


    int input;

    for ( int i = 0; i < size; i ++ )
        {
            for ( int j = 0; j < size; j ++ )
                {
                    cout << "Podaj element ["
                         << i << "][" << j << "] ... ",
                        cin >> input;

                    tab[i][j] = input;
                }
        }


    int highest = tab[0][0];
    int regx = 0;
    int regy = 0;

    for ( int i = 0; i < size; i ++ )
        {
            for ( int j = 0; j < size; j ++ )
                {
                    if ( tab[i][j] > highest )
                        {
                            highest = tab[i][j];
                            regx = i;
                            regy = j;
                        }
                }
        }

    cout << "Największa liczba: " << highest
         << " o indeksach "
         << "[" << regx << "][" << regy << "]"
         << endl;


    return 0;
}
