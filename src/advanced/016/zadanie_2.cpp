/*
 * Maciej Barć ; 64047
 *
 * Temat: długopis
 *
 * 2. Napisz klasę która zawierać będzie:
 *    - 5 atrybutów z których przynajmniej jeden będzie typu string
 *    - Konstruktor z parametrami domyślnymi
 *    - Metodę wypisz – wypisującą wszystkie atrybuty obiektu
 *    - Metodę zmieniającą jeden z atrybutów danego obiektu
 *    - Metodę porównującą dwa obiekty i zwracającą jeden z nich.
 *      (np. porównanie pól figur, wagi zwierząt )
 */


#include <iostream>

#include "dlugopis.hpp"


int main ( )
{
    Dlugopis dlugopis_1;
    Dlugopis dlugopis_2;
    Dlugopis dlugopis_3 ("SPACEX - Militarne Długopisy", 5000);

    std::cout << std::endl;
    dlugopis_1.wypisz();

    std::cout << std::endl;
    dlugopis_2.zmien_firme("Sencer");
    std::cout << "Firma długopisa #2: " << dlugopis_2.firma << std::endl;

    std::cout << std::endl;
    std::cout << "Firma posiadająca dłużej piszący długopis: "
              << Dlugopis::dluzszy_czas_pisania(dlugopis_1, dlugopis_3).firma
              << std::endl;


    return 0;
}
