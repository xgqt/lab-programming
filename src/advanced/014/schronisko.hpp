/*
 * Maciej Barć ; 64047
 *
 * 1. Do projektu z ostatnich zajęć dopisz klasę Kot,
 *    posiadającą atrybuty: imie, wiek, masa.
 *
 * 2. Utwórz konstruktor definiujący wszystkie atrybuty klasy Kot.
 *
 * 3. W  klasie  Kot  dodaj  metodęzmieniającą  imię, oraz metodę wypisującą
 *    na ekran informacje dotyczące danego obiektu.
 *
 * 4. Utwórz klasę Schronisko, posiadającą atrybuty:
 *    nazwa, adres, lista_psow, lista_kotow, ilosc_psow =0, ilosc_kotow=0,
 *    listy mają zawierać po 10 miejsc.
 *
 * 5. Utwórz konstruktor definiujący atrybuty: nazwa, adres,
 *    dla klasy Schronisko.
 *
 * 6. Utwórzmetody void oddaj_psa (Pies p) która będzie dodawać
 *    psa do listy w schronisku, w przypadku braku miejsc,
 *    wypisz na ekraninformacje „Brak miejsc w schronisku”.
 *
 * 7. Utwórz analogiczną metodę void oddaj_kota(Kot k)
 *
 * 8. Utwórz metodę void  adoptuj_psa(Pies  p) którausunie  wskazany obiekt
 *    z listy,  pod warunkiem,że będzie on na liście obecny.
 *    Podpowiedź: Dopisz metodę porównującą obiekty.
 *
 * 9. Utwórz analogiczną metodą void adoptuj_kota(Kot k)
*/


#pragma once

#include <iostream>

#include "kot.hpp"
#include "pies.hpp"


class Schronisko
{
public:

    std::string nazwa;
    std::string adres;

    Kot*  lista_kotow = new Kot[10];
    Pies* lista_psow  = new Pies[10];

    int ilosc_kotow = 0;
    int ilosc_psow  = 0;


    Schronisko ( std::string nazwa, std::string adres );


    void oddaj_kota ( Kot  kot );
    void oddaj_psa  ( Pies pies );

    void adoptuj_kota ( Kot  kot );
    void adoptuj_psa  ( Pies pies );
};
