#include "kot.hpp"
#include <iostream>


Kot::Kot ( std::string imie, int wiek, float masa )
{
    this->imie = imie;
    this->wiek = wiek;
    this->masa = masa;
}


void Kot::zmien_imie ( std::string imie )
{
    this->imie = imie;
}

void Kot::wypisz( )
{
    std::cout << "Imię:" << this->imie << std::endl
              << "Wiek:" << this->wiek << std::endl
              << "Masa:" << this->masa << std::endl;
}

bool Kot::equals( Kot kot )
{
    if (
        this->imie == kot.imie &&
        this->wiek == kot.wiek &&
        this->masa == kot.masa      
        )
        return true;
    else
        return false;
}
