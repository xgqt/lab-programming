#include "pies.hpp"

#include <iostream>


// 01
Pies::Pies ( std::string imie,
             std::string rasa,
             std::string umaszczenie,
             float masa, int wiek )
{
    this->imie = imie;
    this->rasa = rasa;
    this->umaszczenie = umaszczenie;
    this->masa = masa;
    this->wiek = wiek;
}

// 09
Pies::Pies ( std::string imie, std::string umaszczenie,
             float masa, int wiek )
    // 08
    // lista inicjalizacyjna:
    :imie(imie), umaszczenie(umaszczenie), masa(masa), wiek(wiek)
{
    this->rasa = "owczarek niemiecki";
}


// 05
void Pies::wypisz ( )
{
    std::cout << "Imię:        " << this->imie        << std::endl;
    std::cout << "Rasa:        " << this->rasa        << std::endl;
    std::cout << "Umaszczenie: " << this->umaszczenie << std::endl;
    std::cout << "Masa:        " << this->masa        << std::endl;
    std::cout << "Wiek:        " << this->wiek        << std::endl;
}

// 06
void Pies::postarz( )
{
    this->wiek ++;
}

// 07
void Pies::nadaj_imie ( std::string nowe_imie )
{
    this->imie = nowe_imie;
}

// 10
void Pies::nadaj_imie ( )
{
    std::cout << "Podaj nowe imie ... ";
    std::cin >> this->imie;
}

bool Pies::equals( Pies pies )
{
    if (
        this->imie        == pies.imie        &&
        this->rasa        == pies.rasa        &&
        this->umaszczenie == pies.umaszczenie &&
        this->masa        == pies.masa        &&
        this->wiek        == pies.wiek
        )
        return true;
    else
        return false;
}
