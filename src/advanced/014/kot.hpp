#pragma once

#include <iostream>


class Kot
{
public:

    std::string imie;
    int wiek;
    float masa;


    Kot ( std::string imie="Łata",
          int wiek=6, float masa=7.5 );


    void zmien_imie ( std::string );

    void wypisz ( );

    bool equals ( Kot kot );
};
