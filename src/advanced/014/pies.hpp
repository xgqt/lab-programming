#pragma once

#include <iostream>


class Pies
{
public:

    std::string imie;
    std::string rasa;
    std::string umaszczenie;
    float masa;
    int wiek;

    // 01
    Pies ( std::string imie="burek",
           std::string rasa="mieszniec",
           std::string umaszczenie="czarny",
           float masa=10.5, int wiek=6 );

    // 09
    Pies ( std::string imie, std::string umaszczenie,
           float masa, int wiek );


    // 05
    void wypisz ( );
    // 06
    void postarz ( );
    // 07
    void nadaj_imie ( std::string nowe_imie );
    // 10
    void nadaj_imie ( );

    bool equals ( Pies pies );
};
