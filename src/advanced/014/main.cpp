/*
 * Maciej Barć ; 64047
 *
 * 01. Utwórz klasę Pies, posiadającą atrybuty:
 *     imię, rasa, umaszczenie, waga, wiek.
 *
 * 02. Stwórz konstruktor posiadający wszystkie parametry domyślne.
 *
 * 03. Stwórz 3 osbiekty klasy Pies.
 *
 * 04. Czy można stworzyć obiekt podając jedynie ostatni
 *     parametr konstruktora?
 *
 * 05. Stwórz metodę wyświetlającą na ekran wszystkie atrybuty
 *     danego obiektu.
 *
 * 06. Stwórz metodę postarz która postarzy obiekt o jeden rok.
 *
 * 07. Stwórz metodę nadaj_imie która zmieni imię obiektu na wartość
 *     podaną w argumencie metody.
 *
 * 08. Dopisz listę inicjalizacyjną do utworzonego wcześniej konstruktora.
 *
 * 09. Dopisz nowy konstruktor który nie pobierze jako argument rasy.
 *
 * 10. Dokonaj przeciążenia metody nadaj_imietak,by nie podbierała
 *     żadnego argumentu, o imię obiektu zapytaj użytkownika.
 */


#include <iostream>
#include <iterator>
#include <ostream>

#include "pies.hpp"
#include "schronisko.hpp"


int main ( )
{
    Pies pies1_001 ("Azor", "owczarek niemiecki", "biały");

    // 03
    Pies pies1_002 ("Burek");
    Pies pies1_003 ("Reksio");
    Pies pies1_004 ("Rocky");

    Pies pies2_001 ("Bela", "biały", 5, 2);

    std::cout << "Pies 001 - wiek: " << pies1_001.wiek << std::endl;
    std::cout << "Pies 001 - masa: " << pies1_001.masa << std::endl;


    // 05
    std::cout << std::endl;
    pies1_002.wypisz();


    // 06
    std::cout << std::endl;
    std::cout << "Wiek przed: " << pies1_001.wiek << std::endl;

    pies1_001.postarz();
    std::cout << "Wiek po:    " << pies1_001.wiek << std::endl;


    // 07
    std::cout << std::endl;
    std::cout << "Imie przed: " << pies1_001.imie << std::endl;

    pies1_001.nadaj_imie("Nowy");
    std::cout << "Imie po:    " << pies1_001.imie << std::endl;


    // 09
    std::cout << std::endl;
    std::cout << "Rasa psa bez rasy: " << pies2_001.rasa << std::endl;


    // 10
    std::cout << std::endl;
    std::cout << "Imie przed: " << pies1_003.imie << std::endl;

    pies1_003.nadaj_imie();
    std::cout << "Imie po:    " << pies1_003.imie << std::endl;


    Schronisko schronisko_001 ( "Schronikso 7/37", "ul. Rejtana 7/37" );

    schronisko_001.oddaj_psa(pies1_002);
    schronisko_001.oddaj_psa(pies1_003);

    std::cout << schronisko_001.ilosc_psow << std::endl;


    schronisko_001.adoptuj_psa(pies1_002);

    std::cout << schronisko_001.ilosc_psow << std::endl;


    Kot kot1_001;

    schronisko_001.oddaj_kota(kot1_001);
    std::cout << schronisko_001.ilosc_kotow << std::endl;

    schronisko_001.adoptuj_kota(kot1_001);
    std::cout << schronisko_001.ilosc_kotow << std::endl;


    return 0;
}
