#include "schronisko.hpp"
#include <iostream>


Schronisko::Schronisko ( std::string nazwa, std::string adres )
{
    this->nazwa = nazwa;
    this->adres = adres;
}


void Schronisko::oddaj_kota ( Kot kot )
{
    if ( ilosc_kotow < 10 )
        lista_kotow[ilosc_kotow ++] = kot;
    else
        std::cout << "Brak miejsc dla kotów w schronisku";
}

void Schronisko::oddaj_psa ( Pies pies )
{
    if ( ilosc_psow < 10 )
        lista_psow[ilosc_psow ++] = pies;
    else
        std::cout << "Brak miejsc dla psów w schronisku";
}

void Schronisko::adoptuj_psa( Pies pies )
{
    for ( int i = 0; i < ilosc_psow; i ++ )
        {
            if ( lista_psow[i].equals(pies) )
                {
                    for ( int j = 0; j < (ilosc_psow - 1); j ++ )
                        {
                            lista_psow[j] = lista_psow[j + 1];
                        }

                    ilosc_psow --;

                    return;
                }
        }
    std::cout << "Takiego psa nie ma w schronisku." << std::endl;
}

void Schronisko::adoptuj_kota( Kot kot )
{
    for ( int i = 0; i < ilosc_kotow; i ++ )
        {
            if ( lista_kotow[i].equals(kot) )
                {
                    for ( int j = 0; j < (ilosc_kotow - 1); j ++ )
                        {
                            lista_kotow[j] = lista_kotow[j + 1];
                        }

                    ilosc_kotow --;

                    return;
                }
        }
    std::cout << "Takiego psa nie ma w schronisku." << std::endl;
}
