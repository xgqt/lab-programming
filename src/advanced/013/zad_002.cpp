/*
 * Maciej Barć ; 64047
 * 2.
 * Stwórz klasę Trójkąt, posiadającą atrybuty: wysokość,długość podstawyi nazwa
 * a) Stwórz konstruktor parametrowy definiujący wszystkie atrybuty klasy
 * b) Stwórz 3 obiekty tej klasy
 * c) Stwórz metodę, obliczająca pole danego trójkąta
 * d) Stwórz metodę porównująca dwa trójkąty i zwracającą ten obiekt
 *    który ma większe pole
 */


#include <iostream>

#include "zad_002_triangle.hpp"


using namespace std;


int main ( )
{
    Triangle t1 ( 1, 2, "trójkąt #1" );
    Triangle t2 ( 3, 4, "trójkąt #2" );
    Triangle t3 ( 5, 6, "trójkąt #3" );


    std::cout << std::endl;
    std::cout << "Szerokość trójkąta #1: " << t1.width << std::endl;
    std::cout << "Szerokość trójkąta #2: " << t2.width << std::endl;
    std::cout << "Szerokość trójkąta #3: " << t3.width << std::endl;

    std::cout << std::endl;
    t1.write_values();
    t2.write_values();
    t3.write_values();

    std::cout << std::endl;
    std::cout << "Pole trójkąta #1: " << t1.area() << std::endl;
    std::cout << "Pole trójkąta #2: " << t2.area() << std::endl;
    std::cout << "Pole trójkąta #3: " << t3.area() << std::endl;

    std::cout << std::endl;
    std::cout << "Trójkąt o większym polu: "
              << Triangle::compare_area(t1, t2).name << std::endl;


    return 0;
}
