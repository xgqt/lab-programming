/*
 * Maciej Barć ; 64047
 * 4.
 * Napisz funkcję która pobierze prostokąt, trójkąt oraz trapez,
 * a następnie wypisze na ekran pole i nazwę największej figury.
 */


#include <iostream>
#include <iterator>

#include "zad_001_rectangle.hpp"
#include "zad_002_triangle.hpp"
#include "zad_003_trapeze.hpp"


void compare_area ( Rectangle rec, Triangle tri, Trapeze tra )
{
    if ( (rec.area() > tri.area()) && (rec.area() > tra.area()) )
        std::cout << "Największe jest pole prostokąta: "
                  << rec.name << " = " << rec.area() << std::endl;
    else if ( (tri.area() > rec.area()) && (tri.area() > tra.area()) )
        std::cout << "Największe jest pole trójkąta: "
                  << tri.name << " = " << tri.area() << std::endl;
    else
        std::cout << "Największe jest pole trapezu: "
                  << tra.name << " = " << tra.area() << std::endl;
}


int main ( )
{
    Rectangle rectangle ( 4, 4, "prostokąt" );
    Triangle  triangle  ( 4, 4, "trójkąt" );
    Trapeze   trapeze   ( 4, 4, 4, "trapez" );


    compare_area(rectangle, triangle, trapeze);


    return 0;
}
