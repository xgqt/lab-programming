// trójkat


#pragma once

#include <iostream>
#include <string.h>


class Triangle
{
public:

    float height;
    float width;
    std::string name;

    Triangle ( float height, float width, std::string name );

    // wypisane wartości
    void write_values ( );

    // pole
    float area ( );

    // prównainie pól dwóch trójkątów
    static Triangle compare_area ( Triangle t1, Triangle t2 );
};
