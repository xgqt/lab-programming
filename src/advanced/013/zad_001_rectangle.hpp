// prostokąt


#pragma once

#include "zad_002_triangle.hpp"
#include <iostream>
#include <string.h>


class Rectangle
{

public:

    float height;
    float width;
    std::string name;

    Rectangle ( float height, float width, std::string name );

    // wypisane wartości
    void write_values ( );

    // obwód
    float perimeter ( );

    // pole
    float area ( );

    // prównainie obwodów dwóch prostokątów
    static Rectangle compare_perimeter ( Rectangle r1, Rectangle r2 );

    // zmień nazwę
    void set_name ( std::string name );
};
