/*
 * Maciej Barć ; 64047
 * 3.
 * Stwórz klasę Trapez,
 * posiadającą atrybuty: wysokość, podstawa1, podstawa2 i nazwa
 * a) Stwórz konstruktor parametrowy definiujący wszystkie atrybuty klasy
 * b) Stwórz 3 obiekty tej klasy
 * c) Stwórz metodą obliczającą pole danego trapezu
 */


#include <iostream>

#include "zad_003_trapeze.hpp"


int main ( )
{
    Trapeze t1 ( 1, 2, 3, "trapez #1" );
    Trapeze t2 ( 4, 5, 6, "trapez #2" );
    Trapeze t3 ( 7, 8, 9, "trapez #3" );


    std::cout << std::endl;
    t1.write_values();
    t2.write_values();
    t3.write_values();

    std::cout << std::endl;
    std::cout << "Pole trapeza #1: " << t1.area() << std::endl;
    std::cout << "Pole trapeza #2: " << t2.area() << std::endl;
    std::cout << "Pole trapeza #3: " << t3.area() << std::endl;


    return 0;
}
