// trapez


#include "zad_003_trapeze.hpp"


Trapeze::Trapeze ( float height, float base_a, float base_b, std::string name )
{
    this->height = height;
    this->base_a = base_a;
    this->base_b = base_b;
    this->name   = name;
}


// wypisane wartości
void Trapeze::write_values ( )
{
    std::cout << "Nazwa:      " << name   << std::endl;
    std::cout << "Wysokość:   " << height << std::endl;
    std::cout << "Podstawa a: " << base_a << std::endl;
    std::cout << "Podstawa b: " << base_b << std::endl;
}

// pole
float Trapeze::area ( )
{
    return ( this->height * (this->base_a + this->base_b) / 2 );
}
