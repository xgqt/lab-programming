// trapez


#pragma once

#include <iostream>
#include <string.h>


class Trapeze
{
public:

    float height;
    float base_a;
    float base_b;
    std::string name;

    Trapeze ( float height, float base_a, float base_b, std::string name );

    // wypisane wartości
    void write_values ( );

    // pole
    float area ( );
};
