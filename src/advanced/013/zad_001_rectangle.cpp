// prostokąt


#include "zad_001_rectangle.hpp"
#include <string>


Rectangle::Rectangle ( float height, float width, std::string name )
{
    this->height = height;
    this->width  = width;
    this->name   = name;
}


// wypisane wartości
void Rectangle::write_values ( )
{
    std::cout << "Nazwa:     " << name   << std::endl;
    std::cout << "Wysokość:  " << height << std::endl;
    std::cout << "szerokość: " << width  << std::endl;
}

// obwód
float Rectangle::perimeter ( )
{
    return ( ( 2 * this->height ) + ( 2 * this->width ) );
}

// pole
float Rectangle::area ( )
{
    return ( this->width * this->height );
}

// prównainie obwodów dwóch prostokątów
Rectangle Rectangle::compare_perimeter ( Rectangle r1, Rectangle r2 )
{
    return ( r1.perimeter() > r2.perimeter() ? r1 : r2 );
}

// zmień nazwę
void Rectangle::set_name ( std::string name )
{
    this->name = name;
}
