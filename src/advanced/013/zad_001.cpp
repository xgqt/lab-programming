/*
 * Maciej Barć ; 64047
 * 1.
 * Stwórz klasę Prostokąt, posiadającą atrybuty: wysokość, szerokość, nazwa
 * a) Stwórz konstruktor parametrowy definiujący wszystkie atrybuty klasy
 * b) Stwórz 3 obiekty tej klasy
 * c) Stwórz metodę, obliczająca obwód danego prostokąta
 * d) Stwórz metodę, obliczająca pole danego prostokąta
 * e) Stwórz metodę porównująca dwa prostokąty i zwracającą ten obiekt
 *    który ma większe pole
 * f) Stwórz konstruktor, pobierający obwódi obliczający atrybuty tak,
 *    aby prostokąt był możliwie najbardziej podobny do kwadratu
 */


#include <iostream>

#include "zad_001_rectangle.hpp"


int main ( )
{
    Rectangle r1 ( 3.5, 5.3, "prostokąt #1" );
    Rectangle r2 ( 4.5, 6.3, "prostokąt #2" );
    Rectangle r3 ( 5.5, 7.3, "prostokąt #3" );


    std::cout << std::endl;
    std::cout << "Szerokość prostokąta #1: " << r1.width << std::endl;
    std::cout << "Szerokość prostokąta #2: " << r2.width << std::endl;
    std::cout << "Szerokość prostokąta #3: " << r3.width << std::endl;

    std::cout << std::endl;
    r1.write_values();
    r2.write_values();
    r3.write_values();

    std::cout << std::endl;
    std::cout << "Obwód prostokąta #1: " << r1.perimeter() << std::endl;
    std::cout << "Obwód prostokąta #2: " << r2.perimeter() << std::endl;
    std::cout << "Obwód prostokąta #3: " << r3.perimeter() << std::endl;

    std::cout << std::endl;
    std::cout << "Pole prostokąta #1: " << r1.area() << std::endl;
    std::cout << "Pole prostokąta #2: " << r2.area() << std::endl;
    std::cout << "Pole prostokąta #3: " << r3.area() << std::endl;

    std::cout << std::endl;
    std::cout << "Prostokąt o większym obwodzie: "
              << Rectangle::compare_perimeter(r1, r2).name << std::endl;

    std::cout << std::endl;
    r1.set_name("prostokąt #4");
    std::cout << "Nowa nazwa prostokąta #1: " << r1.name << std::endl;


    return 0;
}
