// trójkąt


#include <iostream>

#include "zad_002_triangle.hpp"


Triangle::Triangle ( float height, float width, std::string name )
{
    this->height = height;
    this->width  = width;
    this->name   = name;
}


void Triangle::write_values ( )
{
    std::cout << "Nazwa:     " << name   << std::endl;
    std::cout << "Wysokość:  " << height << std::endl;
    std::cout << "szerokość: " << width  << std::endl;
}

// pole
float Triangle::area ( )
{
    return ( this->width * this->height / 2 );
}

// prównainie dwóch trójkątów
Triangle Triangle::compare_area ( Triangle t1, Triangle t2 )
{
    return ( t1.area() > t2.area() ? t1 : t2 );
}
