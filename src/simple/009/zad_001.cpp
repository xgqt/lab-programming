/*
 * Maciej Barć ; 64047
 * 1.
 * Napisz program pobierający od użytkownika liczbę całkowitą, zapisz ją pod zmienną
 * int liczba. Jeżeli liczba jest dodatnia i parzysta, wypisz na ekran adres tej zmiennej, * w przeciwnym wypadku wypisz na ekran wartość zmiennej liczba.
 */


#include <iostream>


using namespace std;


int main ( )
{
    int liczba;

    cout << "Podaj liczbę ... ",
        cin >> liczba;


    if ( liczba > 0 && liczba % 2 == 0 )
        {
            cout << "Adres liczby: " << & liczba << endl;
        }
    else
        {
            cout << "Wartość liczby: " << liczba << endl;
        }


    return 0;
}
