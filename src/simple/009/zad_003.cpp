/*
 * Maciej Barć ; 64047
 * 3.
 * Dana jest tablica liczb całkowitych z przedziału <1,10>
 *  - Wypisz na ekran adres pierwszego i trzeciego elementu tablicy
 *  - Używając wskaźnika wypisz na ekran 1szy i 3ci element tablicy
 */


#include <iostream>


using namespace std;


int main ( )
{
    int tab[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };


    cout << "Adres pierwszego elementu: " << & tab[0] << endl;
    cout << "Adres trzeciego  elementu: " << & tab[2] << endl;

    int * first  = & tab[0];
    int * second = & tab[2];
    cout << "Wartość pierwszego elementu: " << * first  << endl;
    cout << "Wartość trzeciego  elementu: " << * second << endl;


    return 0;
}
