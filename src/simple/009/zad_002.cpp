/*
 * Maciej Barć ; 64047
 * 2.
 * Dana jest zmienna float liczba = 8.1
 * Napisz program który przekaże tą zmienną do wskaźnika, oraz przy jego użyciu:
 *  - Wypisze na ekran wartość zmiennej liczba
 *  - Wypisze na ekran adres zmiennej liczba
 *  - Wypisze na ekran adres wskaźnika
 */


#include <iostream>


using namespace std;


int main ( )
{
    float liczba = 8.1;


    float * pnt = & liczba;

    cout << liczba << endl;

    cout << & liczba << endl;

    cout << & pnt << endl;


    return 0;
}
