/*
 * Maciej Barć ; 64047
 * 4.
 * Dane są zmienne:
 * int a = 4;
 * int b = 8;
 * int * wsk1 = & a;
 * int * wsk2 = & b;
 * Napisz program który:
 *  - Używając wskaźników dokonaj sumowania liczb a i b, a wynik zapisze pod zmienną suma
 *  - Używając wskaźników dokona mnożenia liczb ai b, a wynik zapisze pod zmienną iloczyn
 *  - Zapisze adres zmiennej sumado wsk1, a adres zmiennej iloczyn do wsk2
 *  - Wyświetli na ekran wartości zmiennych, których adresy przechowywane są pod
 *    wskaźnikami
 */


#include <iostream>


using namespace std;


int main ( )
{
    int a = 4;
    int b = 8;
    int * wsk1 = & a;
    int * wsk2 = & b;

    int suma;
    int iloczyn;


    suma = * wsk1 + * wsk2;

    iloczyn = * wsk1 * * wsk2;

    wsk1 = & suma;
    wsk2 = & iloczyn;

    cout << "Wskaźnik wsk1: " << * wsk1 << endl;
    cout << "Wskaźnik wsk2: " << * wsk2 << endl;


    return 0;
}
