/*
 * Maciej Barć ; 64047
 * 2.
 * Napisz program wyznaczający NWD(a,b) metodą rekurencyjną.
 */


#include <iostream>


using namespace std;


int nwd ( int a, int b )
{
    if ( a != b )
        {
            return nwd (
                        a > b ? a - b : a,
                        b > a ? b - a : b
                        );
        }
    else
        {
            return a;
        }
}


int main ( )
{
    int a, b;

    cout << "Podaj wartość a ... ",
        cin >> a;
    cout << "Podaj wartość b ... ",
        cin >> b;


    cout << endl << "NWD " << a << " i " << b << " to: "
         << nwd(a, b) << endl;


    return 0;
}
