/*
 * Maciej Barć ; 64047
 * 6.
 * Napisz funkcję, która zamienia duże litery (od A do Z) na małe.
 * Parametrem funkcji jest znak, funkcja zwraca zamieniony znak.
 * Jeśli zostanie podany innymi znak niż litera to funkcja
 * zwraca znak bez zmiany.
 */


#include <iostream>
#include <string.h>


using namespace std;


char downcase ( char znak )
{
    if ( isalpha(znak) )
        {
            return znak + 32;
        }
    else
        {
            return znak;
        }
}


int main ( )
{
    char str;

    cout << "Podaj znak ... ",
        cin >> str;


    cout << endl << "Znak: "
         << downcase(str) << endl;


    return 0;
}
