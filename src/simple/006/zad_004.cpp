/*
 * Maciej Barć ; 64047
 * 4.
 * Napisz program, który wyznaczy silnię z liczby n sposobem rekurencyjnym.
 */


#include <iostream>


using namespace std;


int silnia ( int num )
{
    if ( num < 0 )
        {
            return 0;
        }
    if ( num == 0 )
        {
            return 1;
        }
    else
        {
            return num * (num - 1) ;
        }
}


int main ( )
{
    int x;

    cout << "Podaj wartość x ... ",
        cin >> x;


    cout << endl << "Silnia z " << x << ": "
         << silnia(x) << endl;


    return 0;
}
