/*
 * Maciej Barć ; 64047
 * 5.
 * Poniżej  zdefiniowany  jest  pewien  ciąg,
 * którego  kolejne  wyrazy  generowane  są wsposób rekurencyjny:
 * a(n) =
 *     0                 dla n=1
 *     0,5               dla n=2
 *     -a(n-1) * a(n-2)  dla n>2
 */


#include <iostream>


using namespace std;


float ciag ( float num )
{
    if ( num == 1 )
        {
            return 0;
        }
    if ( num == 2 )
        {
            return 0.5;
        }
    else
        {
            return - ciag(num - 1) * ciag(num - 2);
        }
}


int main()
{
    int x;

    cout << "Podaj wartość x ... ",
        cin >> x;


    cout << endl << "ciag(x) to: "
         << ciag(x) << endl;


    return 0;
}
