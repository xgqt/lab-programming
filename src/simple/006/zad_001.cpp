/*
 * Maciej Barć ; 64047
 * 1.
 * Wykorzystując funkcje rekurencyjną wykonaj zamianę liczby
 * w systemie dziesiętnym na system dwójkowy.
 */


#include <iostream>
#include <math.h>


using namespace std;


int bin = 0, i = 0;

int binarna ( int num )
{
    if ( num > 0 )
        {
            bin += (num % 2) * pow(10, i);
            num = num / 2;
            i ++;

            return binarna(num);
        }
    else
        {
            return bin;
        }
}


int main()
{
    int x;

    cout << "Podaj liczbę w systemie dziesiętnym ... ",
        cin >> x;


    cout << "Liczba binarna z " << x << ": "
         << binarna(x) << endl;


    return 0;
}
