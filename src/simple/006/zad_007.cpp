/*
 * Maciej Barć ; 64047
 * 7.
 * Napisz funkcję, która ma trzy parametry formalne a, b, c
 * będące liczbami całkowitymi. Wartością funkcji jest jeden,
 * jeśli zadane liczby są liczbami pitagorejskimi
 * oraz zero w przeciwnym wypadku.
 * Liczby pitagorejskie spełniają warunek: a*a+b*b=c*c.
 */


#include <iostream>


using namespace std;


bool pitagoras ( int a, int b, int c )
{
    if ( a*a + b*b == c*c )
        {
            return 1;
        }
    else
        {
            return 0;
        }
}


int main ( )
{
    int a, b, c;

    cout << "Podaj wartość a ... ",
        cin >> a;
    cout << "Podaj wartość b ... ",
        cin >> b;
    cout << "Podaj wartość c ... ",
        cin >> c;


    if ( pitagoras(a, b, c) )
        {
            cout << endl <<  "Są to liczby pitagorejskie." << endl;
        }
    else {
        cout << endl << "Nie są to liczby pitagorejskie." << endl;
    }


    return 0;
}
