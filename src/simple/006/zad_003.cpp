/*
 * Maciej Barć ; 64047
 * 3.
 * Napisz funkcje rekurencyjną wyznaczająca ciąg Fibanaccego.
 */


#include <iostream>


using namespace std;


int fib ( int num )
{
    if ( num == 0 )
        {
            return 0;
        }
    if ( num == 1 )
        {
            return 1;
        }
    else
        {
            return fib(num - 1) + fib(num - 2);
        }
}


int main ( )
{
    int x;

    cout << "Podaj wartość x ... ",
        cin >> x;


    cout << endl << "fib(" << x << ") to: "
         << fib(x) << endl;


    return 0;
}
