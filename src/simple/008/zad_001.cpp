/*
 * Maciej Barć ; 64047
 * Kolokwium_2020_D
 * 1.
 */


#include <iostream>


using namespace std;


int main ( )
{
    int n;
    int liczba_ujemnych=0, liczba_dodatnich=0;
    float suma_ujemnych=0, suma_dodatnich=0;
    float z;

    cout << "Podaj n ... ",
        cin >> n;

    if ( n <= 0 )
        {
            cout << "n musi być większe od 0" << endl;
            return 0;
        }


    for ( int i = 0; i < n; i ++ )
        {
            cout << "Podaj liczbe nr " << i+1 << endl;
            cin >> z;

            // zero nie jest ani dodatnie ani ujemne
            if ( z > 0 )
                {
                    liczba_dodatnich ++;
                    suma_dodatnich += z;
                }
            else if ( z < 0 )
                {
                    liczba_ujemnych ++;
                    suma_ujemnych += z;
                }
        }


    cout << "Liczba liczb dodatnich: "
         << liczba_dodatnich << endl;
    cout << "Liczba liczb ujemnych: "
         << liczba_ujemnych << endl;

    cout << "Suma liczb dodatnich: "
         << suma_dodatnich << endl;
    cout << "Suma liczb ujemnych: "
         << suma_ujemnych << endl;

    cout << "Suma wszystkich podanych liczb: "
         <<  suma_dodatnich + suma_ujemnych << endl;


    return 0;
}
