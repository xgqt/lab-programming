/*
 * Maciej Barć ; 64047
 * Kolokwium_2020_D
 * 2.
 */


#include <iostream>


using namespace std;


int main ( )
{
    int n, m;
    double R = 0, iloczyn = 1;

    cout << "Podaj n ... ",
        cin >> n;
    cout << "Podaj m ... ",
        cin >> m;

    cout << endl;


    for ( int i = 1; i <= n; i ++ )
        {
            iloczyn = 1;

            for ( int j = 1; j <= m; j ++ )
                {
                    iloczyn = iloczyn *
                        ( i*i + j*j - 1 ) / ( 2*i + 3*j + 4 );

                    cout << "Iloczyn nr " << j << ": " <<
                        iloczyn << endl;
                }

            R = R + iloczyn;
            cout << "Suma nr " << i << ": "
                 << R << endl;
        }

    cout << endl << "R jest równe: "
         << R << endl;


    return 0;
}
