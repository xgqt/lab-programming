/*
 * Maciej Barć ; 64047
 * 1.
 * Dana jest funkcja
 * float bigger ( float a, float b )
 * {
 *    return a > b ? a : b;
 * }
 * Utwórz wskaźnik funkcyjny i przypisz do niego adres powyższej funkcji
 * Wywołaj funkcję przy użyciu wskaźnika
 */


#include <iostream>


using namespace std;


float bigger ( float a, float b )
{
    return a > b ? a : b;
}


int main ( )
{
    float ( *bigger_ptr ) ( float, float );
    bigger_ptr = bigger;

    cout << bigger_ptr(3, 9) << endl;


    return 0;
}
