/*
 * Maciej Barć ; 64047
 * 2.
 * Utwórz wskaźnik funkcyjny do którego przypiszesz wskaźnik na funkcję sqrt()
 * znajdującą się w bibliotece math
 */


#include <iostream>
#include <math.h>


using namespace std;


int main ( )
{
    // sqrt(double __x)
    double ( *sqrt_ptr ) ( double );
    sqrt_ptr = sqrt;

    cout << sqrt_ptr(9) << endl;


    return 0;
}
