/*
 * Maciej Barć ; 64047
 * 4.
 * Napisz funkcję mul_bigger która w parametrach przyjmie wskaźnik na funkcję
 * z zadania pierwszego oraz parametry niezbędne do jej wywołania.
 * Zadaniem nowej funkcji będzie przemnożenie wartości zwróconej przez funkcje
 * z zadania 1 przez wartość podaną w ostatnim parametrze funkcji mul_bigger
 */


#include <iostream>


using namespace std;


float bigger ( float a, float b )
{
    return a > b ? a : b;
}

float mul_bigger ( float ( *fun_ptr ) ( float, float ),
                   float a, float b, float mul )
{
    return fun_ptr(a, b) * mul;
}


int main ( )
{
    cout << mul_bigger(bigger, 3, 5, 6) << endl;


    return 0;
}
