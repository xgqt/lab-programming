/*
 * Maciej Barć ; 64047
 * 3.
 * Utwórz wskaźniki na wszystkie funkcje utworzone podczas laboratoriów 3 (011).
 */


#include <cmath>
#include <iostream>


using namespace std;


void div2 ( int& x )
{
    x /= 2;
}

int* age ( int wiek )
{
    cout << "Deref. wiek: " << &wiek << endl;

    return &wiek;
}

void hmx2 ( int tab[], int size )
{
    int max = tab[0];
    int idx = 0;

    for ( int i = 0; i < size; i ++ )
        {
            if ( max < tab[i] )
                {
                    max = tab[i];
                    idx = i;
                }
        }

    tab[idx] = pow(tab[idx], 2);
}


int main ( )
{
    void ( *div2_ptr ) ( int& );
    div2_ptr = div2;

    int num = 6;
    div2_ptr(num);
    cout << num << endl;


    int* ( *age_ptr ) ( int );
    age_ptr = age;

    cout << age_ptr(67) << endl;


    void ( *hmx2_ptr ) ( int*, int );
    hmx2_ptr = hmx2;

    int tab[] = { 1, 2, 3 };
    hmx2_ptr(tab, (sizeof(tab) / sizeof(int)));
    cout << endl;
    for ( auto i : tab )
        {
            cout << i << "  ";
        }
    cout << endl;


    return 0;
}
