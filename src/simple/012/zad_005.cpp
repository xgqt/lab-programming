/*
 * Maciej Barć ; 64047
 * 5.
 * Przekształć funkcję z zadania pierwszego tak, aby pobierała liczby przez
 * referencję oraz zwracała wskaźnik na większą z nich.
 * Przekaż wartości do funkcji, bez użycia referencji. Co się zmieniło?
 */


#include <iostream>


using namespace std;


float *bigger ( float &a, float &b )
{
    return a > b ? &a : &b;
}


int main ( )
{
    float *( *bigger_ptr ) ( float &, float & );
    bigger_ptr = bigger;

    float num1 = 3;
    float num2 = 9;

    cout << "Addr num1: " << &num1 << endl;
    cout << "Addr num2: " << &num2 << endl;

    cout << "Addr of bigger: " <<  bigger_ptr(num1, num2) << endl;
    cout << "Val of bigger:  " << *bigger_ptr(num1, num2) << endl;


    return 0;
}
