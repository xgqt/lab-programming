/*
 * Maciej Barć ; 64047
 * 10.
 * Napisz program, w którym zaprezentujesz użycie obiektu klasy std::string
 * w celu inicjalizacji, przechowywania danych wejściowych użytkownika oraz kopiowania,
 * łączenia i określenia długości ciągu tekstowego.
 */


#include <iostream>
#include <string>


using namespace std;


int main ( )
{
    string tekst;
    string tekst_;

    cout << "Wpisz tekst ... ";
    getline(cin, tekst);


    tekst_ = tekst;
    tekst_.append(tekst_);

    cout << tekst << endl;
    cout << "Długośc '" << tekst_ << "': " << tekst_.size() << endl;


    return 0;
}
