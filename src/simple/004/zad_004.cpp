/*
 * Maciej Barć ; 64047
 * 4.
 * Napisz program znajdujący minimalną wartość w tablicy n-elementowej,
 * elementy tablicy są losowane z przedziału [-23, 50].
 */


#include <iostream>
#include <time.h>


using namespace std;


int main ( )
{
    srand(int(time(NULL)));

    int n, min;

    cout << "Podaj wartośc n (długość tablicy) ... ";
    cin >> n;

    int tab[n];


    for ( int i = 0; i < n; i++ )
        {
            tab[i] = rand() % 74 - 23;
        }


    cout << endl << "Elementy tablicy:" << endl;
    for ( int i = 0; i < n; i ++ )
        {
            cout << i << ": " << tab[i] << endl;
        }

    
    min = tab[0];
    for ( int i = 0; i < n; i ++ )
        {
            if ( tab[i] < min )
                {
                    min = tab[i];
                }
        }
    cout << endl << "Minimalna wartość tablicy: " << min << endl;
    
    
    return 0;
}
