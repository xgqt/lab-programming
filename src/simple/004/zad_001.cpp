/*
 * Maciej Barć ; 64047
 * 1.
 * Napisz program wprowadzający do 10-elementowej tablicy
 * wartości: 3, 6, 9, ...
 */


#include <iostream>


using namespace std;


int main ( )
{
    int tab[10];


    // od 3 do 30
    for ( int i = 0; i < 10; i ++ )
        {
            tab[i] = 3 + 3 * i;
        }


    for ( int i = 0; i < 10; i ++ )
        {
            cout << i << ": " << tab[i] << endl;
        }


    return 0;
}
