/*
 * Maciej Barć ; 64047
 * 12.
 * Napisz program sprawdzający ile jest spółgłosek i samogłosek we wprowadzonym tekście.
 */


#include <iostream>
#include <string>


using namespace std;


int main ( )
{
    string spolgloski_tab = "bcdfghjklmnpqrstvwxz";
    string samogloski_tab = "aeiouy";
    int samogloski = 0, spolgloski = 0;
    string tekst;

    cout << "Wprowadź tekst ... ";
    getline(cin, tekst);


    for ( char l : tekst )
        {
            for ( char a : samogloski_tab )
                {
                    if ( l == a )
                        {
                            samogloski ++;
                        }
                }
            for ( char p : spolgloski_tab )
                {
                    if ( l == p )
                        {
                            spolgloski ++;
                        }
                }
        }

    cout << "Samogłoski: " << samogloski << endl;
    cout << "Spółgłoski: " << spolgloski << endl;


    return 0;
}
