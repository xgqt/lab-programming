/*
 * Maciej Barć ; 64047
 * 6.
 * Dana jest tablica 20 elementowa. 
 * Pierwsza połowa tablicy jest wypełniona. 
 * Napisz program, który wypełni drugą połowę tablicy poprzez 
 * kopiowanie kolejnych wartości z pierwszej połowy tablicy.
 */


#include <iostream>
#include <time.h>


using namespace std;


int main ( )
{
    srand(int(time(NULL)));

    const int tl = 20;
    int tab[tl];

    for ( int i = 0; i < ( tl / 2 ); i ++ )
        {
            tab[i] = rand() % 41 - 20;
        }

    
    for ( int i = ( tl / 2 ); i < tl; i ++ )
        {
            tab[i] = tab[i - ( tl / 2 )];
        }

    
    cout << endl << "Elementy tablicy:" << endl;
    for ( int i = 0; i < tl; i ++ )
        {
            cout << i << ": " << tab[i] << endl;
        }

    
    return 0;
}
