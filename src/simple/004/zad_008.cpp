/*
 * Maciej Barć ; 64047
 * 8.
 * Napisz program dodawania dwóch macierzy.
 */


#include <iostream>
#include <time.h>


using namespace std;


int main ( )
{
    srand(int(time(NULL)));

    int x = 5, y = 5;
    int mx1[x][y];
    int mx2[x][y];
    int mx3[x][y];

    for ( int i = 0; i < x; i ++ )
        {
            for ( int j = 0; j < y; j ++ )
                {
                    mx1[i][j] = rand() % 41 - 20;
                }
        }

    for ( int i = 0; i < x; i ++ )
        {
            for ( int j = 0; j < y; j ++ )
                {
                    mx2[i][j] = rand() % 41 - 20;
                }
        }


    cout << "Elementy macierzy #1:" << endl;

    for ( int i = 0; i < x; i ++ )
        {
            for ( int j = 0; j < y; j ++ )
                {
                    cout << i << "x" << j << ": " << mx1[i][j] << endl;
                }
        }

    cout << "Elementy macierzy #2:" << endl;

    for ( int i = 0; i < x; i ++ )
        {
            for ( int j = 0; j < y; j ++ )
                {
                    cout << i << "x" << j << ": " << mx2[i][j] << endl;
                }
        }


    for ( int i = 0; i < x; i ++ )
        {
            for ( int j = 0; j < y; j ++ )
                {
                    mx3[i][j] = mx1[i][j] + mx2[i][j];
                }
        }


    cout << endl << "Elementy sumy tych macierzy:" << endl;

    for ( int i = 0; i < x; i ++ )
        {
            for ( int j = 0; j < y; j ++ )
                {
                    cout << i << "x" << j << ": " << mx3[i][j] << endl;
                }
        }


    return 0;
}
