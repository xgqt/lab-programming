/*
 * Maciej Barć ; 64047
 * 3.
 * Wypełnij n-elementową tablicę, zaczynając od ostatniej komórki tablicy,
 * wartościami a, a-5, a-10, ... .
 * Wartość całkowitą a podaje użytkownik.
 * Kolejna wartość ma być obliczana na podstawie wpisanej
 * w poprzednim kroku wartości do tablicy.
 */


#include <iostream>


using namespace std;


int main ( )
{
    int n, a;

    cout << "Podaj wartośc n (długość tablicy) ... ";
    cin >> n;
    cout << "Podaj wartośc a ... ";
    cin >> a;

    int tab[n];


    for ( int i = n - 1 ; i >= 0; i -- )
        {
            tab[i] = a;
            a -= 5;
            cout << i << ": " << tab[i] << endl;
        }

    return 0;
}
