/*
 * Maciej Barć ; 64047
 * 11.
 * Napisz program, który prosi o podanie informacji:
 *   Podaj nazwę ulicy na której mieszkasz: Stefana Batorego
 *   Podaj nr domu: 43/15
 *   Na ile oceniasz swoje umiejętności programowania w skali [2 - 5]: -4
 *   Twój staż programistyczny: 5
 * A potem wyświetla następujący informacje:
 *   Adres: Stefana Batorego 43/15
 *   Ocena: 2
 *   Staż: 5 dni
 * Program powinien przyjmować adres składający się więcej niż z jednego wyrazu.
 * Dodatkowo ocena podana przez użytkownika powinna być przy wyświetleniu mniejsza o 1.
 */


#include <iostream>
#include <string>


using namespace std;


int main ( )
{
    string ulica, nrdomu;
    int ocena, dni;

    cout << "Podaj nazwę ulicy na której mieszkasz: ";
    getline(cin, ulica);
    cout << "Podaj nr domu: ";
    getline(cin, nrdomu);
    cout << "Na ile oceniasz swoje umiejętności programowania w skali [2 - 5]: ";
    cin >> ocena;
    cout << "Twój staż programistyczny: ";
    cin >> dni;


    // z przykładu: -4 -> 2
    if ( ocena < 2 )
        {
            ocena  = 2;
        }

    // ocena podana przez użytkownika powinna być PRZY WYŚWIETLENIU mniejsza o 1
    ocena --;

    cout << endl;
    cout << "Adres: " << ulica << " " << nrdomu << endl;
    cout << "Ocena: " << ocena << endl;
    cout << "Staż: " << dni << " dni" << endl;


    return 0;
}
