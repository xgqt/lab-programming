/*
 * Maciej Barć ; 64047
 * 5.
 * Użytkownik  podaje  2  pierwsze  wartości  tablicy.
 * Wypełnij  pozostałe  komórki  tablicy wartościami,
 * które stanowią sumę wartości z dwóch poprzedzających komórek tablicy.
 * np.: [ 3, 6, ?, ?, ? ] -> [ 3, 6, 9, 15, 24 ]
 */


#include <iostream>


using namespace std;


int main ( )
{
    const int tl = 5;
    int tab[tl];

    cout << "Podaj pierwszy element tablicy ... ";
    cin >> tab[0];
    cout << "Podaj drugi element tablicy ... ";
    cin >> tab[1];


    // Start od drugiego indexu
    for ( int i = 2; i < tl; i ++ )
        {
            tab[i] = tab[i - 1] + tab[i - 2];
        }


    cout << endl << "Elementy tablicy:" << endl;
    for ( int i = 0; i < tl; i ++ )
        {
            cout << i << ": " << tab[i] << endl;
        }


    return 0;
}
