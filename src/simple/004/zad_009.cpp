/*
 * Maciej Barć ; 64047
 * 9.
 * Dana  jest  deklaracja  tablicy:
 * char tab[]  = "wiosna";
 * napisz program, który policzy i wyświetli na konsoli liczbę znaków w danej tablicy.
 */


/*
 * Zadanie 1.
 *   Czym różni się tablica zadeklarowana w sposób:
 *   char zdanie[] = {"lot"};
 *   od tablicy:
 *   char zdanie[] = { 'l', 'o', 't'};
 *
 * W drugiej tablicy brakuje znaka końca linii '\0'.
 * Pierwwsza tablica ma długość 3 a druga 6

 * Zadanie 2.
 *   Czy  poniższy  fragment  kodu  jest  poprawny?
 *   Co  można  powiedzieć  o  wczytywaniu elementów do tablic?
 *   int liczby[100];
 *   char znaki[100];
 *   cin >> liczby;
 *   cin >> znaki;
 *
 * Nie, nie można wczytać w sposób "cin >> liczby;" do tablicy liczb,
 * tylko do tablicy łańcuchów
 */


#include <iostream>
#include <string.h>


using namespace std;


int main ( )
{
    char tab[]  = "wiosna";


    cout << "Liczba znaków tablicy 'tab': " << strlen(tab) << endl;


    return 0;
}
