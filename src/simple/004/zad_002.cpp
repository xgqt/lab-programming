/*
 * Maciej Barć ; 64047
 * 2.
 * Do programu z zadania 1 dodaj instrukcje wyświetlające 
 * co drugą wartość z tablicy.
 */


#include <iostream>


using namespace std;


int main ( )
{
    int tab[10];


    // od 3 do 30
    for ( int i = 0; i < 10; i ++ )
        {
            tab[i] = 3 + 3 * i;
        }


    // pierwsza = 0 i druga = 1
    for ( int i = 1; i < 10; i = i + 2 )
        {
            cout << i << ": " << tab[i] << endl;
        }


    return 0;
}
