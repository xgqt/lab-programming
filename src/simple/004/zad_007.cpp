/*
 * Maciej Barć ; 64047
 * 7.
 * Napisz program obliczania sumy elementów przekątnej głównej macierzy.
 */


#include <iostream>
#include <time.h>


using namespace std;


int main ( )
{
    srand(int(time(NULL)));

    int x = 5, y = 5;
    int mx[x][y];
    int sum = 0;

    for ( int i = 0; i < x; i ++ )
        {
            for ( int j = 0; j < y; j ++ )
                {
                    mx[i][j] = rand() % 41 - 20;
                }
        }


    cout << "Elementy tablicy:" << endl;

    for ( int i = 0; i < x; i ++ )
        {
            for ( int j = 0; j < y; j ++ )
                {
                    cout << i << "x" << j << ": " << mx[i][j] << endl;
                }
        }


    for ( int i = 0; i < x; i ++ )
        {
            sum += mx[i][i];
        }

    cout << "Suma elementów przekątnej: " << sum << endl;


    return 0;
}
