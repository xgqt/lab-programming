/*
 * Maciej Barć ; 64047
 * 1.
 * Napisz funkcje obliczające pole i objętość:
 * a.koła b.prostokąta c.trójkąta d.sześcianu e.walca
 */


#include <iostream>


using namespace std;


double pole_kola ( double r )
{
    return 3.14 * r * r;
}

float pole_prostokata ( float a, float b )
{
    return a * b;
}

double pole_trojkata ( double a, double h )
{
    return (a * h) / 2;
}

float pole_szescianu ( float a )
{
    return 6 * a * a;
}

float pole_walca ( float r, float h )
{
    return 2 * 3.14 * r * r + 2 * 3.14 * r * h;
}

float objetosc_szescianu ( float a )
{
    return a * a * a;
}


int main ( )
{
    cout << "Pole koła          (r=6):      "
         << pole_kola(6) << endl;

    cout << "Pole prostokąta    (a=6, b=3): "
         << pole_prostokata(6, 3) << endl;

    cout << "Pole trójkąta      (a=6, h=3): "
         << pole_trojkata(6, 3) << endl;

    cout << "Pole sześcianu     (a=6):      "
         << pole_szescianu(6) << endl;

    cout << "Pole walca         (r=6, h=3): "
         << pole_walca(6, 3) << endl;

    cout << "Objętość sześcianu (a=6):      "
         << objetosc_szescianu(6) << endl;


    return 0;
}
