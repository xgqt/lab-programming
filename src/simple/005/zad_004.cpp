/*
 * Maciej Barć ; 64047
 * 4.
 * Napisz funkcję, która dla nieujemnej liczby n, takiej, że 0 <= n <= 30
 * wynwyznaczy dwusilnię według następującego wzoru:
 * n!! = {
 *         1                                dla  n = 0
 *         n * (n-2) * (n-4) * ... * 4 * 2  dla  n parzystego dodatniego
 *         n * (n-2) * (n-4) * ... * 3 * 1  dla  n nieparzystego
 * }
 */


#include <iostream>


using namespace std;


long dwusilnia ( int n )
{
    long dw = 1;

    if ( n == 1 )
        {
            return 0;
        }
    else
        {
            for ( int i = n; i > 1; i -= 2 )
                {
                    dw *= i;
                }
            return dw;
        }
}


int main ( )
{
    for ( int i = 0; i < 31; i ++ )
        {
            cout << i << "!! = " << dwusilnia(i) << endl;
        }


    return 0;
}
