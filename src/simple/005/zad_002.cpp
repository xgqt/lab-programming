/*
 * Maciej Barć ; 64047
 * 2.
 * Napisz implementację funkcji bool LiczbaPierwsza(int liczba).
 * Liczba pierwsza to taka liczba naturalna większa od 1,
 * która dzieli siętylko przez 1 i samą siebie.
 */


#include <iostream>


using namespace std;


bool LiczbaPierwsza ( int liczba )
{
    if ( liczba <= 1 )
        {
            return false;
        }
    else
        {
            for ( int i = 2; i < liczba; i ++ )
                {
                    if ( liczba % i  == 0 )
                        {
                            return false;
                        }
                }
            return true;
        }
}


int main ( )
{
    cout << "0  :" << LiczbaPierwsza(0) << endl;
    cout << "1  :" << LiczbaPierwsza(1) << endl;
    cout << "7  :" << LiczbaPierwsza(7) << endl;
    cout << "12 :" << LiczbaPierwsza(12) << endl;
    cout << "17 :" << LiczbaPierwsza(17) << endl;
    cout << "24 :" << LiczbaPierwsza(24) << endl;


    return 0;
}
