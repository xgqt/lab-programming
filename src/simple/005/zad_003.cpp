/*
 * Maciej Barć ; 64047
 * 3.
 * Napisz funkcję, która wyświetli postać binarną podanej liczby
 * całkowitej zapisanej w systemie dziesiętnym.
 */


#include <iostream>
#include <math.h>


using namespace std;


int binarna ( int liczba )
{
    int bin = 0;
    int i = 0;

    while ( liczba > 0 )
        {
            bin += (liczba % 2) * pow(10, i);
            liczba = liczba / 2;
            i ++;
        }

    return bin;
}


int main ( )
{
    for ( int i = 0; i < 25; i ++ )
        {
            cout << i << ": " << binarna(i) << endl;
        }


    return 0;
}
