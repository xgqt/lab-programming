/*
 * Maciej Barć ; 64047
 * 7.
 * Z wykorzystaniem operatorów logicznych !(negacja), || (or) napisz program,
 * który w zależności od spełnienia pewnych warunków wyświetla odpowiednie komunikaty:
 * - Jeśli nie ma zniżki na samochód lub otrzymałeś premię,
 *   to "Możesz kupić samochód!", "Zniżki na samochód nie ma :("
 * - Jeśli nie ma zniżki na samochód lub nie otrzymałeś premii,
 *   to "Zakup samochód trzeba odłożyć na później...", "Zniżki na samochód nie ma"
 * - Jeśli jest zniżka na samochód lub otrzymałeś premię,
 *   to "Możesz kupić samochód!"
 * Użytkownik podaje informacje o tymczy jest zniżka na samochód i czy otrzymałeś podwyżkę.
 */


#include <iostream>


using namespace std;


int main ( )
{
    bool premia, znizka;

    cout << "Czy otrzymałeś premie (0/1) ... ";
    cin >> premia;
    cout << "Czy jest znizka na samochód (0/1) ... ";
    cin >> znizka;

    if ( ! znizka || premia )
        {
            cout << "Możesz kupić samochód!" << endl;
            cout << "Zniżki na samochód nie ma :(" << endl;
        }
    else if ( ! znizka || ! premia )
        {
            cout << "Zakup samochód trzeba odłożyć na później..." << endl;
            cout << "Zniżki na samochód nie ma." << endl;
        }
    else if ( znizka || premia )
        {
            cout << "Możesz kupić samochód!" << endl;
        }

    return 0;
}
