/*
 * Maciej Barć ; 64047
 * 3.
 * Napisz program rozwiązywania równania
 * kwadratowego ax^2 + bx + c = 0 ,
 * gdzie a, b i csą dowolnymi stałymi podawanymi przez użytkownika.
 */


#include <iostream>
#include <cmath>


using namespace std;


int main ( )
{

    float a, b, c, x0, x1, x2, delta;

    cout << "Podaj a ... ";
    cin >> a;
    cout << "Podaj b ... ";
    cin >> b;
    cout << "Podaj c ... ";
    cin >> c;

    delta = b * b - 4 * a * c;

    if ( delta > 0 )
        {
            delta = sqrt(delta);
            x1 = ( -b - delta ) / ( 2 * a );
            x2 = ( -b + delta ) / ( 2 * a );
            cout << "Pierwiastki równania: " << x1 << " oraz " << x2 << endl;
        }
    else
        if ( delta == 0 )
            {
                x0 = -b / ( 2 * a );
                cout << "Pierwiastki równania: " << x0 << endl;
            }
        else
            {
                cout << "Równanie nie ma pierwiastków" << endl;
            }

    return 0;
}
