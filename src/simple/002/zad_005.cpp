/*
 * Maciej Barć ; 64047
 * 5.
 * Napisz program porządkowania trzech liczb x, y i z.
 * Od najmniejszej do największej.
 */


#include <iostream>


using namespace std;


int main ( )
{
    float x, y, z;

    cout << "Podaj x ... ";
    cin >> x ;
    cout << "Podaj y ... ";
    cin >> y;
    cout << "Podaj z ... ";
    cin >> z;

    if ( x > y && x > z )
        {
            if ( y > z )
                {
                    cout << z << " , " << y << " , " << x << endl;
                }
            else
                {
                    cout << y << " , " << z << " , " << x << endl;
                }
        }
    if ( y > x && y > z )
        {
            if ( x > z )
                {
                    cout << z << " , " << x << " , " << y << endl;
                }
            else
                {
                    cout << y << " , " << z << " , " << z << endl;
                }
        }
    if( z > x && z > y )
        {
            if ( x > y )
                {
                    cout << y << " , " << x << " , " << z << endl;
                }
            else
                {
                    cout << x << " , " << y << " , " << z << endl;
                }
        }

    return 0;
}
