/*
 * Maciej Barć ; 64047
 * 1.
 * Napisz programrozwiązywania równania liniowego ax + b = 0 ,
 * gdzie a i b są dowolnymi stałymi podawanymi przez użytkownika.
 */


#include <iostream>


using namespace std;


int main ( )
{
    float a, b;

    cout << "Podaj a ... ";
    cin >> a;
    cout << "Podaj b ... ";
    cin >> b;

    if ( a == 0 )
        {
            cout << "Funkcja tożsamościowa" << endl;
        }
    else if ( b == 0 )
        {
            cout << "Funkcja stała" << endl;
            cout << "x = " << b << endl;
        }
    else
        {
            cout << "x = " << -b / a << endl;
        }

    return 0;
}
