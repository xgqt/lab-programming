/*
 * Maciej Barć ; 64047
 * 6.
 * Z wykorzystaniem operatorów logicznych ! (negacja), && (and) napisz program,
 * który w zależności od spełnienia pewnych warunków wyświetla odpowiednie komunikaty:
 * - Jeśli pada deszcz i jest autobus na przystanku,
 *   to "Weź parasol", "Dostaniesz się na uczelnie"
 * - Jeśli pada deszcz i nie ma autobusu,
 *   to "Nie dostaniesz się na uczelnię";
 * - Jeśli nie pada deszcz i jest autobus na przystanku,
 *   to "Dostaniesz się na uczelnie", "Miłego dnia i pięknej pogody"
 * Użytkownik podaje informacje o tym czy pada i czy jest autobus.
 */


#include <iostream>


using namespace std;


int main ( )
{
    bool pada, bus;

    cout << "Czy pada (0/1) ... ";
    cin >> pada;
    cout << "Czy a przystaku jest autobus (0/1) ... ";
    cin >> bus;

    if ( pada && bus )
        {
            cout << "Weż parasol." << endl;
            cout << "Dostaniesz się na uczelnie." << endl;
        }
    else if ( pada && ! bus )
        {
            cout << "Nie dostaniesz sie na uczelnie." << endl;
        }
    else if ( ! pada && bus )
        {
            cout << "Dostaniesz sie na uczelnie." << endl;
            cout << "Miłego dnia i pięknej pogody." << endl;
        }

    return 0;
}
