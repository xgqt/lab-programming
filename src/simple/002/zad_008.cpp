/*
 * Maciej Barć ; 64047
 * 8.
 * Napisz program działający jak prosty kalkulator,
 * który potrafidodawać, odejmować, mnożyć, dzielić i obliczać resztę z dzielenia.
 */


#include <iostream>


using namespace std;


int main ( )
{
    char oper;
    float x, y;
    int ix, iy;

    cout << "Jaki typ obliczenia chcesz wykonac? ('+', '-', '*' , '/') ... ";
    cin >> oper;
    cout << "Podaj x ... ";
    cin >> x;
    cout << "Podaj y ... ";
    cin >> y;
    cout << endl;

    ix = x;
    iy = y;

    switch ( oper )
        {
        case '+':
            cout << x << " + " <<  y << " = ";
            cout << x + y << endl;
            break;

        case '-':
            cout << x << " - " <<  y << " = ";
            cout << x - y << endl;
            break;

        case '*':
            cout << x << " * " <<  y << " = ";
            cout << x * y << endl;
            break;

        case '/':
            cout << ix << " / " <<  iy << " = ";
            cout << ix / iy << endl;
            cout << "reszta: " << ix % iy << endl;
            break;

        default:
            cout << "Zły znak operacji" << endl;
            break;
        }

    return 0;
}
