/*
 * Maciej Barć ; 64047
 * 2.
 * Napisz program określania ilości pierwiastków równania
 * kwadratowego ax^2 + bx + c = 0 ,
 * gdzie a, b i csą dowolnymi stałymi podawanymi przez użytkownika.
 */


#include <iostream>


using namespace std;


int main ( )
{

    double a, b, c, delta;

    cout << "Podaj a ... ";
    cin >> a;
    cout << "Podaj b ... ";
    cin >> b;
    cout << "Podaj c ... ";
    cin >> c;

    delta = b * b - 4 * a * c;

    if ( delta > 0 )
        {
            cout << "Równaie ma dwa pierwiastki" << endl;
        }
    else
        if ( delta == 0 )
            {
                cout << "Równaie ma jeden pierwiastek" << endl;
            }
        else
            {
                cout << "Równanie nie ma pierwiastków" << endl;
            }

    return 0;
}
