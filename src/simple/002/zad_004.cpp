/*
 * Maciej Barć ; 64047
 * 4. 
 * Napisz program wyznaczania wartości funkcji określonych wzorami 
 * dla argumentów rzeczywistych podawanych przez użytkownika
 */


#include <iostream>


using namespace std;


int main ( )
{

    float x;

    cout << "Podaj wartość x ... ";
    cin >> x;

    // a(x)
    // 2 * x  , x >  0
    // 0      , x == 0
    // -3 * x , x <  0
    cout << "Funkcja a(x)" << endl;
    if ( x > 0 )
        {
            cout << "2 * x = " << 2 * x << endl;
        }
    else if ( x == 0 )
        {
            cout << "x = " << 0 << endl;
        }
    else
        {
            cout << "-3 * x = " << -3 * x << endl;
        }
    cout << endl;

    // b(x)
    // x ^ 2  , x >= 1
    // x      , x <  1
    cout << "Funkcja b(x)" << endl;
    if ( x >= 1 )
        {
            cout << "x^2 = " << x * x << endl;
        }
    else
        {
            cout << "x = " << x << endl;
        }
    cout << endl;

    // c(x)
    // 2 + x  , x >  2
    // 8      , x == 2
    // x - 4  , x <  2
    cout << "Funkcja c(x)" << endl;
    if ( x > 2 )
        {
            cout << "2 + x = " << 2 + x << endl;
        }
    else if ( x == 2 )
        {
            cout << "x = " << 8 << endl;
        }
    else
        {
            cout << "x - 4 = " << x - 4 << endl;
        }
    cout << endl;

    return 0;
}
