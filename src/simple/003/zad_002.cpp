/*
 * Maciej Barć ; 64047
 * 1.
 * Napisz program, w którym użytkownik podaje liczby
 * do momentu wpisania wartości mniejszej od 0.
 */


#include <iostream>


using namespace std;


int main ( )
{
    float x = 0;


    do
        {
            cout << "Podaj wartość liczby x ... " << endl;
            cin >> x;
        }
    while ( x >= 0 );


    return 0;
}
