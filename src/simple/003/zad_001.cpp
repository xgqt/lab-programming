/*
 * Maciej Barć ; 64047
 * 1.
 * Napisać program, który dla wprowadzonej z klawiatury liczby naturalnej
 * wypisze wszystkie liczby ją poprzedzające zaczynając od 0.
 * (for, do while, while)
 */


#include <iostream>


using namespace std;


int main ( )
{
    int x;
    int j = 0, k = 0;

    cout << "Podaj wartość liczby x ... ";
    cin >> x;
    cout << endl;


    cout << "Pętla 'for'" << endl;
    for ( int i = 0; i < x; i ++ )
        {
            cout << i << endl;
        }
    cout << endl;

    cout << "Pętla 'do while'" << endl;
    do
        {
            cout << j << endl;
            j ++;
        }
    while ( j < x );
    cout << endl;

    cout << "Pętla 'while'" << endl;
    while ( k < x )
        {
            cout << k << endl;
            k ++;
        }
    cout << endl;


    return 0;
}
