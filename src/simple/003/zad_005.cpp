/*
 * Maciej Barć ; 64047
 * 1.
 * Wykorzystując algorytm z zadania 4,
 * napisz program który pozwoli użytkownikowi na wylosowanie n liczb
 * z przedziału ( -10, 45 )
 * i obliczy sumę wszystkich liczb parzystych w danym ciągu.
 */


#include <iostream>
#include <stdlib.h>
#include <time.h>


using namespace std;


int main ( )
{
    srand(int(time(NULL)));

    int n, num, sum = 0;

    cout << "Podaj wartość n będącą ilością liczb ciągu ... ";
    cin >> n;
    cout << endl;

    if ( n <= 0 )
        {
            cout << "Podałeś błędną długość ciągu: " << n << endl;
            cout << "Jest ona mniejsza lub równa zero." << endl;
            exit(0);
        }


    for ( int i = 0; i < n; i ++ )
        {
            num = rand() % 45 - 10;

            cout << "Wylosowano liczbę numer " << i+1
                 << " równą: " << num << endl;

            if ( num % 2 == 0)
                {
                    cout << " + Dodaję liczbę: "
                         << num << " do sumy." << endl;
                    sum += num;
                }
        }

    cout << endl << "Suma podanego ciągu " << n
         << " liczb jest równa: " << sum << endl;


    return 0;
}
