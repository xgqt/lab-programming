/*
 * Maciej Barć ; 64047
 * 1.
 * Dany jest ciąg n liczb (n>0), napisz program pozwalający na obliczenie
 * sumy wszystkich liczb parzystych w danym ciągu.
 */


#include <iostream>


using namespace std;


int main ( )
{
    int n, num, sum = 0;

    cout << "Podaj wartość n będącą ilością liczb ciągu ... ";
    cin >> n;
    cout << endl;

    if ( n <= 0 )
        {
            cout << "Podałeś błędną długość ciągu: " << n << endl;
            cout << "Jest ona mniejsza lub równa zero." << endl;
            exit(0);
        }


    for ( int i = 0; i < n; i ++ )
        {
            cout << "Podaj libcbę numer " << i+1 << " ... ";
            cin >> num;

            if ( num % 2 == 0)
                {
                    sum += num;
                }
        }

    cout << endl << "Suma podanego ciągu " << n
         << " liczb jest równa: " << sum << endl;


    return 0;
}
