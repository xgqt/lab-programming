/*
 * Maciej Barć ; 64047
 * 1.
 * Napisz program, który pozwoli na wczytanie 10 liczb
 * i wyznaczeniu ilości oraz sumy liczb ujemnych i dodatnich
 * podanych przez użytkownika.
 */


#include <iostream>


using namespace std;


int main ( )
{
    float num;
    int       u = 0 ,    d = 0;
    float sum_u = 0, sum_d = 0;


    for ( int i = 0 ; i < 10; i ++ )
        {
            cout << "Podaj numer " << i << " ... ";
            cin >> num;
        if ( num < 0 )
            {
                u ++;
                sum_u += num;
            }
        if ( num > 0 )
            {
                d ++;
                sum_d += num;
            }
        }

    cout << endl;
    cout << "Ilosć liczb ujemnych:  " << u << endl;
    cout << "Suma  liczb ujemnych:  " << sum_u << endl;
    cout << "Ilosć liczb dodatnich: " << d << endl;
    cout << "Suma  liczb dodatnich: " << sum_d << endl;


    return 0;
}
