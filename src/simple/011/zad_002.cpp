/*
 * Maciej Barć ; 64047
 * 2.
 * Napisz funkcję, która będzie aktualizowała wartość zmiennejprzekazanej
 * przez parametr do funkcji, wykonując dzielenie przez 2.
 * Jako parametr przekaż:
 *   - Zmienną wartościową
 *   - Wskaźnik
 *   - Referencję
 * Które ze zmiennych zostały zmienione po uruchomieniu funkcji?
 */


#include <iostream>


using namespace std;


void div2 ( int & x )
{
    x /= 2;
}


int main ( )
{
    int num       = 120;
    int * num_ptr = & num;
    int & num_ref = num;

    cout << "num:     " << num     << endl;
    cout << "num_ptr: " << num_ptr << endl;
    cout << "num_ref: " << num_ref << endl;


    cout << endl << "Zmienna:" << endl;

    div2(num);

    cout << "num:     " << num     << endl;
    cout << "num_ptr: " << num_ptr << endl;
    cout << "num_ref: " << num_ref << endl;


    cout << endl << "Wskaznik:" << endl;

    div2(* num_ptr);

    cout << "num:     " << num     << endl;
    cout << "num_ptr: " << num_ptr << endl;
    cout << "num_ref: " << num_ref << endl;


    cout << endl << "Referencja:" << endl;

    div2(num_ref);

    cout << "num:     " << num     << endl;
    cout << "num_ptr: " << num_ptr << endl;
    cout << "num_ref: " << num_ref << endl;


    return 0;
}
