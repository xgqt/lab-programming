/*
 * Maciej Barć ; 64047
 * 1.
 * Napisz program, w którym utworzysz zmienną typu float, oraz zmienną
 * referencyjną z nią powiązaną. Sprawdź jak zmiana wartości wpływa na
 * zmienną referencyjną (powiązaną).
 */


#include <iostream>


using namespace std;


int main ( )
{
    float num = 1.99;
    float & num_ref = num;

    cout << "num:     " << num << endl;
    cout << "num_ref: " << num_ref << endl;


    cout << endl << "num + 2" << endl;

    num += 2;

    cout << "num:     " << num << endl;
    cout << "num_ref: " << num_ref << endl;


    cout << endl << "num_ref + 2" << endl;

    num_ref += 2;

    cout << "num:     " << num << endl;
    cout << "num_ref: " << num_ref << endl;


    return 0;
}
