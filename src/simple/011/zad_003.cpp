/*
 * Maciej Barć ; 64047
 * 3.
 * Napisz funkcję, pobierającą zmienną wiek. Funkcja zwracać będzie
 * wskaźnik do tej zmiennej.Sprawdź czy rzeczywiście adres zmiennej
 * przekazywanej do funkcji i zwracanej z funkcji jest taki sam.
 */


#include <iostream>


using namespace std;


int * age ( int wiek )
{
    cout << "Deref. wiek: " << & wiek << endl;

    return & wiek;
}


int main ( )
{
    int w = 30;


    cout << & w << endl;

    cout << age(w) << endl;


    return 0;
}
