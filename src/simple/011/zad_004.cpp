/*
 * Maciej Barć ; 64047
 * 4.
 * Napisz funkcję pobierającą tablicę liczb całkowitych i podnoszącą
 * do kwadratunajwiększą liczbę w tablicy.
 * Pamiętaj, że tablice przekazywane są do funkcji przez referencje.
 */


#include <cmath>
#include <iostream>


using namespace std;


void hmx2 ( int tab[], int size )
{
    int max = tab[0];
    int idx = 0;

    for ( int i = 0; i < size; i ++ )
        {
            if ( max < tab[i] )
                {
                    max = tab[i];
                    idx = i;
                }
        }

    tab[idx] = pow(tab[idx], 2);
}


int main()
{
    int tab[] = {1, 2, 33, 4, 5};

    for ( auto i : tab )
        {
            cout << i << "  ";
        }


    hmx2(tab, (sizeof(tab) / sizeof(int)));

    cout << endl;
    for ( auto i : tab )
        {
            cout << i << "  ";
        }
    cout << endl;


    return 0;
}
