/*
 * Maciej Barć ; 64047
 * 1.
 * Napisz program pobierający od użytkownika liczbę całkowitą,
 * utwórz tablicę dynamiczną o tylu elementach. Wpisz do tablicy kolejne
 * liczby całkowite dodatnie. Czy można utworzyć „zwyczajną” tablicę o
 * liczbie elementów podanej przez użytkownika?
 */


#include <iostream>


using namespace std;


int main ( )
{
    int len;

    cout << "Podaj rozmiar tablicy ... ",
        cin >> len;


    int * tab = new int [ len ];

    if ( len <= 0 )
        {
            exit(0);
        }


    for ( int i = 0; i < len; i ++ )
        {
            tab [i] = i + 1;
        }


    for ( int i = 0; i < len; i ++ )
        {
            cout << i << ": " << tab [i] << endl;
        }


    delete [] tab;

    return 0;
}
