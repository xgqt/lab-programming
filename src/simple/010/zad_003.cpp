/*
 * Maciej Barć ; 64047
 * 3.
 * Wykonaj zadanie drugie używając notacji wskaźnikowej.
 */


#include <iostream>


using namespace std;


int main ( )
{
   int side;

    cout << "Podaj bok macierzy ... ",
        cin >> side;


    // 0. tworzenie tablicy

    // tablica wskaźników

    int ** tab = new int * [ side ];

    // utworzenie wierszy

    for ( int i = 0; i < side; i ++ )
        {
            * ( tab + i ) = new int [side];
        }

    // zapełnienie tablicy

    int cntr = 3;

    for ( int i = 0; i < side; i ++ )
        {
            for ( int j = 0; j < side; j ++ )
                {
                    cout << i << ":" << j << " = " << cntr << endl;

                    * ( * ( tab + j ) + i ) = cntr;
                    cntr += 3;
                }
        }


    // 1. suma

    int sum = 0;

    for ( int i = 0; i < side; i ++ )
        {
            for ( int j = 0; j < side; j ++ )
                {
                    sum += * ( * ( tab + j ) + i );
                }
        }

    cout << "Suma elementów: " << sum << endl;



    // 2. digonalna
    //    inny sposób

    float sumd = 0;

    for ( int i = 0; i < side; i ++ )
        {
            sumd += * ( * ( tab + i ) + i );
        }

    cout << "Średnia elementów na przekątnej: " << sumd / side << endl;


    // 3. max

    int max = 0;

    for ( int i = 0; i < side; i ++ )
        {
            for ( int j = 0; j < side; j ++ )
                {
                    if ( tab [i] [j] > max )
                        max = * ( * ( tab + j ) + i );
                }
        }

    cout << "Maksimum macierzy: " << max << endl;


    return 0;
}
