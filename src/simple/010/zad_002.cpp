/*
 * Maciej Barć ; 64047
 * 2.
 * Utwórz macierz kwadratową o długości boku podanej przez użytkownika.
 * Do macierzy wpisz kolejne liczby całkowite dodatnie podzielne przez 3,
 * macierz uzupełniaj wierszami. Zadanie wykonaj używając notacji
 * tablicowej.
 *  1. Policz sumę wszystkich elementów w tablicy
 *  2. Policz średnią elementów znajdujących się na przekątnej diagonalnej
 *  3. Wyznacz maksimum w macierzy
 */


#include <iostream>


using namespace std;


int main ( )
{
   int side;

    cout << "Podaj bok macierzy ... ",
        cin >> side;


    // 0. tworzenie tablicy

    // tablica wskaźników

    int ** tab = new int * [ side ];

    // utworzenie wierszy

    for ( int i = 0; i < side; i ++ )
        {
            tab [i] = new int [side];
        }

    // zapełnienie tablicy

    int cntr = 3;

    for ( int i = 0; i < side; i ++ )
        {
            for ( int j = 0; j < side; j ++ )
                {
                    cout << i << ":" << j << " = " << cntr << endl;

                    tab [i] [j] = cntr;
                    cntr += 3;
                }
        }


    // 1. suma

    int sum = 0;

    for ( int i = 0; i < side; i ++ )
        {
            for ( int j = 0; j < side; j ++ )
                {
                    sum += tab [i] [j];
                }
        }

    cout << "Suma elementów: " << sum << endl;



    // 2. digonalna

    float sumd = 0;

    for ( int i = 0; i < side; i ++ )
        {
            for ( int j = 0; j < side; j ++ )
                {
                    if ( i == j )
                        sumd += tab [i] [j];
                }
        }

    cout << "Średnia elementów na przekątnej: " << sumd / side << endl;


    // 3. max

    int max = 0;

    for ( int i = 0; i < side; i ++ )
        {
            for ( int j = 0; j < side; j ++ )
                {
                    if ( tab [i] [j] > max )
                        max = tab [i] [j];
                }
        }

    cout << "Maksimum macierzy: " << max << endl;


    return 0;
}
