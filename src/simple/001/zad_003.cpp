/*
 * Maciej Barć ; 64047
 * 1.
 * Napisać program, który pozwala użytkownikowi na wprowadzenie
 * dwóch liczb zmiennoprzecinkowych, a następnie zwraca informację,
 * która z wprowadzonych liczb jest liczbą większą.
 */


#include <iostream>


using namespace std;


int main ( )
{
    float x, y;

    cout << "Podaj wartosć zmiennej x ... ";
    cin >> x;
    cout << "Podaj wartosć zmiennej y ... ";
    cin >> y;


    if ( x > y )
        {
            cout << "Liczba x równa '" << x << "' jest większa." << endl;
        }
    else if ( y > x )
        {
            cout << "Liczba y równa '" << y << "' jest większa." << endl;
        }
    else
        {
            cout << "Liczby x i y są obie równe '" << x << "'." << endl;
        }


    return 0;
}
