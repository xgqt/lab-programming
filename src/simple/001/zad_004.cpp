/*
 * Maciej Barć ; 64047
 * 1.
 * Napisać prosty kalkulator wykonujący działanie matematyczne (+, -, /, *) 
 * na dwóch liczbach podanych z klawiatury, wykorzystując instrukcję switch.
 */


#include <iostream>


using namespace std;


int main ( )
{
    char oper;
    float x, y;
    int ix, iy;

    cout << "Jaki typ obliczenia chcesz wykonac? ('+', '-', '*' , '/') ... ";
    cin >> oper;
    cout << "Podaj x ... ";
    cin >> x;
    cout << "Podaj y ... ";
    cin >> y;
    cout << endl;

    ix = x;
    iy = y;

    switch ( oper )
        {
        case '+':
            cout << x << " + " <<  y << " = ";
            cout << x + y << endl;
            break;

        case '-':
            cout << x << " - " <<  y << " = ";
            cout << x - y << endl;
            break;

        case '*':
            cout << x << " * " <<  y << " = ";
            cout << x * y << endl;
            break;

        case '/':
            cout << ix << " / " <<  iy << " = ";
            cout << ix / iy << endl;
            cout << "reszta: " << ix % iy << endl;
            break;

        default:
            cout << "Zły znak operacji" << endl;
            break;
        }

    return 0;
}
