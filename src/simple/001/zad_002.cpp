/*
 * Maciej Barć ; 64047
 * 1.
 * Napisać program, który sprawdza, czy podana liczba całkowita,
 * wczytana z klawiatury jest parzysta, czy nieparzysta. (operator %).
 */


#include <iostream>


using namespace std;


int main ( )
{
    int num;

    cout << "Podaj liczbę ... ";
    cin >> num;


    cout << "Podana liczba '" << num << "' jest ";

    if ( num % 2 == 0 )
        {
            cout << "parzysta" << endl;
        }
    else
        {
            cout << "nieparzysta" << endl;
        }


    return 0;
}
