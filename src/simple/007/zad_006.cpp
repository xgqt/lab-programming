/*
 * Maciej Barć ; 64047
 * 6.
 * Opracować program, który oblicza wartość sumy (n - int, a – double)
 * S = sum( (a^3 - 7) / (i^2 + 1) )
 */


#include <iostream>
#include <cmath>


using namespace std;


int main ( )
{
    int n;
    double a, sum=0;

    cout << "Podaj długosc ciagu (n) ... ",
        cin >> n;
    cout << "Podaj liczbę (a) ... ",
        cin >> a;

    cout << endl;


    for ( int i = 0; i < n; i ++ )
        {
            sum += ( a * pow(i, 3) - 7 ) / ( pow(i, 2) + 3 );

            cout << "sum(" << i << ") : " << sum << endl;
        }

    cout << endl << "Suma: " << sum << endl;


    return 0;
}
