/*
 * Maciej Barć ; 64047
 * 8.
 * Opracować program, który wczytuje dwie tablice jednowymiarowe A, B
 * obie n-elementowe (n < 100) zawierające liczby typu double
 * i następnie wyprowadza elementy tablicy C zdefiniowane jako:
 * Ci =
 *      2 * Ai + Bi + 1  gdy  Ai >  Bi
 *      Ai - Bi -1       gdy  Ai <= Bi
 * Kolejność danych : n, A0, ... An-1, B0, ... Bn-1
 * (n : int, pozostałe : double)
 */


#include <iostream>


using namespace std;


int main ( )
{
    int n;

    cout << "Podaj wilkość tablic ... ",
        cin >> n;

    double A [n];
    double B [n];
    double C [n];

    // A0, ..., An-1
    for ( int i = 0; i < n; i ++ )
        {
            cout << "Podaj liczbę " << i
                 << " dla tablicy A ... ",
                cin >> A[i];
        }
    // B0, ..., Bn-1
    for ( int i = 0; i < n; i ++ )
        {
            cout << "Podaj liczbę " << i
                 << " dla tablicy B ... ",
                cin >> B[i];
        }


    for ( int i = 0; i < n; i ++ )
        {
            if ( A[i] > B[i] )
                {
                    C[i] = 2 * A[i] + B[i] + 1;
                }
            else
                {
                    C[i] = A[i] - B[i] - 1;
                }
        }


    cout << "Zawartośc tablicy C:";

    for ( int i = 0; i < n; i ++ )
        {
            cout << " " << C[i];
        }

    cout << endl;


    return 0;
}
