/*
 * Maciej Barć ; 64047
 * 3.
 * Napisz funkcję, która wyznacza n-tą (n jest liczba naturalną)
 * potęgę zadanej liczby rzeczywistej x.
 * Funkcja ma wykorzystywać rekurencję.
 */


#include <iostream>
#include <string>


using namespace std;


int to_pow ( int num, int pow, int res )
{
    if ( pow == 0 )
        {
            return 1;
        }
    else if ( pow > 1 )
        {
            res *= num;
            return to_pow(num, pow-1, res);
        }
    else
        {
            return res;
        }
}


int main ( )
{
    int x, n;

    cout << "Podaj liczbę ... ",
        cin >> x;
    cout << "Podaj potęgę ... ",
        cin >> n;


    cout << "Liczba " << x << " do potęgi "
         << n << " to: " << to_pow(x, n+1, 1) << endl;


    return 0;
}
