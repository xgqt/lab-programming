/*
 * Maciej Barć ; 64047
 * 1.
 * Napisz funkcję, która stwierdza, czy zadana jako parametr liczba
 * całkowita jest kwadratem pewnej liczby całkowitej.
 * Liczby będące kwadratami liczb całkowitych to 1, 4, 9, 16 itd.
 * Wartością funkcji ma być jeden, jeśli liczba spełnia warunek
 * oraz zero w przeciwnym wypadku.
 */


#include <iostream>
#include <cmath>


using namespace std;


int is_power ( int num )
{
    if ( num == (sqrt(num) * sqrt(num)) )
        {
            return 1;
        }
    else
        {
            return 0;
        }
}


int main ( )
{
    int x;

    cout << "Podaj liczbę ... ",
        cin >> x;


    if ( is_power(x) == 1 )
        {
            cout << "Liczba jest kwadratem pewnej liczby całkowitej."
                 << endl;
        }
    else
        {
            cout << "Liczba nie jest kwadratem liczby całkowitej."
                 << endl;
        }


    return 0;
}
