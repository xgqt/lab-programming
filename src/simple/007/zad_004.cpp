/*
 * Maciej Barć ; 64047
 * 4.
 * Opracować program, który prowadzi grę "zgadnij liczbę".
 * Zgadywane mają być liczby z zakresu [0, 9].
 * Program powinien informować o powtórnym wprowadzeniu tej samej liczby.
 */


#include <iostream>


using namespace std;


int main ( )
{
    srand(time(NULL));

    int tab[20], b;
    b = rand()% 10;

    for ( int i = 0; i < 20; i ++ )
        {

            cout << "Podaj liczbe ... ",
                cin >> tab[i];

            if ( tab[i] == b )
                {
                    cout << "Liczba zostala zgadnięta" << endl;
                    break;
                }

            for ( int p = 0; p < i; p ++ )
                {
                    if ( tab[i] == tab[p] )
                        {
                            cout << "Już wczesniej podałeś tą liczbę"
                                 << endl;
                            break;
                        }
                }
        }


    return 0;
}
