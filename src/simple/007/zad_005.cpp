/*
 * Maciej Barć ; 64047
 * 5.
 * Opracować program, który wczytuje dwie tablice jednowymiarowe A, B
 * obie n-elementowe (n < 100) zawierające liczby typu double
 * i następnie wyprowadza ich elementy w kolejności:
 * A0B0...An-1Bn-1.
 * Kolejność danych : n, A0, ... An-1, B0, ... Bn-1
 * (n : int, pozostałe : double)
 */


#include <iostream>


using namespace std;


int main ( )
{
    int n;

    cout << "Podaj wilkość tablic ... ",
        cin >> n;

    double tab1 [n];
    double tab2 [n];

    for ( int i = 0; i < n; i ++ )
        {
            cout << "Podaj liczbę " << i
                 << " dla tablicy tab1 ... ",
                cin >> tab1[i];

            cout << "Podaj liczbę " << i
                 << " dla tablicy tab2 ... ",
                cin >> tab2[i];
        }


    cout << "Liczby:";

    for ( int i = 0; i < n; i ++ )
        {
            cout << " " << tab1[i] << " " << tab2[i];
        }

    cout << endl;


    return 0;
}
