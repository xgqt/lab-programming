/*
 * Maciej Barć ; 64047
 * 7.
 * Opracować program, który oblicza wartość iloczynu (m – int, I – double)
 * I = prod( sqrt( (j^2 + 2j - 1) / (3j - 1)) )
 */


#include <iostream>
#include <cmath>


using namespace std;


int main ( )
{
    int n;
    double a, iloczyn=1;

    cout << "Podaj długosc ciagu (n) ... ",
        cin >> n;
    cout << "Podaj liczbę (a) ... ",
        cin >> a;

    cout << endl;


    for ( int i = 0; i < n; i ++ )
        {
            iloczyn *= sqrt( (pow(i, 2) + 2*i - 1) / (3*i - 1) );

            cout << "iloczyn(" << i << ") : " << iloczyn << endl;
        }

    cout << endl << "Iloczyn: " << iloczyn << endl;


    return 0;
}
