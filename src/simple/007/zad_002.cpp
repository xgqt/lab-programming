/*
 * Maciej Barć ; 64047
 * 2.
 * Napisz funkcję, która wyznacza n-tą (n jest liczba naturalną)
 * potęgę zadanej liczby rzeczywistej x.
 * Funkcja ma wykorzystywać iterację.
 */


#include <iostream>


using namespace std;


int main ( )
{
    int x, x_, n;

    cout << "Podaj liczbę ... ",
        cin >> x;
    cout << "Podaj potęgę ... ",
        cin >> n;

    x_ = x;


    for ( int i = 1; i < n; i ++ )
        {
            x = x * x_;
        }


    cout << "Liczba " << x_ << " do potęgi "
         << n << " to: " << x << endl;


    return 0;
}
