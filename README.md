# LAB - Programming

Source code for small exercises I did on programming laboratories.


# Topics

- [Loops](./src/simple/003)
- [Strings](./src/simple/004)
- [Functions pt. 1](./src/simple/005)
- [Functions pt. 2](./src/simple/006)
- [Arrays pt. 1](./src/simple/007)
- [Variable pointers pt. 1](./src/simple/009)
- [Arrays pt. 2](./src/simple/010)
- [Variable pointers pt. 2](./src/simple/011)
- [Function pointers](./src/simple/012)
- [Classes](./src/advanced/013)
- [Overloading](./src/advanced/014)
- [STL: vectors, stack, queue](./src/advanced/015)
- [Summarizing Exercises](./src/advanced/016)


# License

SPDX-License-Identifier: GPL-3.0-only & CC0-1.0

Scripts with the GPL header in this repository are licensed under the appropriate license (GPL-3.0-only).

The C/C++ source files are licensed under the CC0 1.0 Universal license.

## Unless otherwise stated contents here are under the GNU GPL v3 license

This file is part of lab-programming.

lab-programming is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

lab-programming is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with lab-programming.  If not, see <https://www.gnu.org/licenses/>.

Copyright (c) 2021, Maciej Barć <xgqt@riseup.net>
Licensed under the GNU GPL v3 License
